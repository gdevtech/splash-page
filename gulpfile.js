// Require the Main Module
var gulp = require('gulp'),
// Other Node Modules
   stylus       = require('gulp-css-preprocessor'),
   pug          = require('gulp-pug'),
   concat       = require('gulp-concat'),
   minify       = require('gulp-uglify'),
   rename       = require('gulp-rename'),
   browserSync  = require('browser-sync');

// Compile Stylus
gulp.task('stylus', function(){
    return gulp.src('assets/css/main.styl')
        .pipe(stylus())
        .pipe(gulp.dest('assets/css'))
});

// Compile Pug
gulp.task('pug', function(){
    return gulp.src('views/index.pug')
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest('./'))
});

// Concate & Minify
gulp.task('scripts', function(){
    return gulp.src(['assets/js/lib/*'])
        .pipe(concat('master.js'))
        .pipe(gulp.dest('assets/js'))
        .pipe(rename('master.min.js'))
        .pipe(minify())
        .pipe(gulp.dest('assets/js'))
});

//Watch Files
gulp.task('watch', function(){
    // Watch All Stylus
    gulp.watch([
        'assets/css/main.styl',
        'assets/css/components/*.styl',
        'assets/css/global/*.styl',
        'assets/css/modules/*.styl',
        'assets/css/pages/*.styl',
    //Watch Pug Files
        'views/layout/*.pug',
        'views/partials/*.pug'
    ],
    [
        'stylus',
        'pug',
        'sync'
    ]);
    //Watch js Files
    gulp.watch('assets/js/*.js',['scripts']);
    //Connect Live
    browserSync({
        server: {
            baseDir: './'
        }
    });
});

//Connect Live
gulp.task('sync', ['stylus', 'pug', 'scripts'], browserSync.reload);

//Default Task
gulp.task('default', ['stylus', 'pug', 'scripts', 'watch', 'sync']);